all: clock_gettime LD_PRELOAD_clock_gettime.so \
  rdtsc rdtsc-sigprocmask LD_PRELOAD_rdtsc.so \
  fake-rdtsc-ptrace

# CFLAGS += -g
CFLAGS += -O2
CFLAGS += -Wall

LD_PRELOAD_%.so: LD_PRELOAD_%.c
	$(CC) $(CFLAGS) -fPIC -shared -o $@ $< -ldl
